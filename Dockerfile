FROM jitsi/web:latest
COPY title.html /usr/share/jitsi-meet/
COPY images /usr/share/jitsi-meet/images
COPY css /usr/share/jitsi-meet/css
COPY libs /usr/share/jitsi-meet/libs
COPY favicon.ico /usr/share/jitsi-meet/
COPY apple-app-site-association  /usr/share/jitsi-meet/
COPY interface_config.js /defaults
